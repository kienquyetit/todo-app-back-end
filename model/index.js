const mongoose = require("mongoose")

mongoose.connect("mongodb://localhost:27017/TodoApp", { useNewUrlParser: true, useUnifiedTopology : true }, (err) => {
    if(!err) {
        console.log("Success Connected")
    } else {
        console.log("Error connecting to database")
    }

})

const Todo = require("./todo.model")