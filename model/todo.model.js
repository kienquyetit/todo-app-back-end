const mongoose = require('mongoose')

var TodoSchema = new mongoose.Schema({
    name: {
        type: String
    },
    isDone: {
        type: Boolean
    },
    isRemoved: {
        type: Boolean
    },
    isEdit: {
        type: Boolean
    }
})

mongoose.model("Todo", TodoSchema)