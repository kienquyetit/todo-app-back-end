const connection = require("./model")
const express = require("express")
const application = express()
const path = require("path")
const Handlebars = require('handlebars')
const expressHandlebars = require("express-handlebars")
const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access')
const bodypasser = require("body-parser")

const TodoController = require("./controllers/todo")

application.use(bodypasser.urlencoded({
    extended: true
}))

application.set("views", path.join(__dirname, "/views/"))

application.engine("hbs", expressHandlebars({
    extname: "hbs",
    defaultLayout: "mainlayout",
    layoutsDir: __dirname + "/views/layouts",
    handlebars: allowInsecurePrototypeAccess(Handlebars)
}))

application.set("view engine", "hbs")

application.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

application.use("/todo", TodoController)

application.listen("3000", () => {
    console.log("Server started")
})