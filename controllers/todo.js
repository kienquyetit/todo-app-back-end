const express = require("express")
const mongoose = require("mongoose")

const router = express.Router()
const TodoModel = mongoose.model("Todo")
mongoose.set('useFindAndModify', false);

router.post("/post", (req, res) => {

    var todo = new TodoModel()
    todo.name = req.body.name
    todo.isDone = req.body.isDone
    todo.isRemoved = req.body.isRemoved
    todo.isEdit = req.body.isEdit
    todo.save((err, doc) => {
        if (!err) {
            // res.redirect("/course/list")
            res.json(doc)
        } else {
            res.send("Error Occured")
        }
    })
})

router.get("/", async (req, res) => {
    try {
        var todoList = await TodoModel.find()
        // res.render("list", { data: docs })
        res.json(todoList)
    } catch (error) {
        res.json({ message: todoList })
    }

})

router.put("/:id", async (req, res) => {
    try {
        let updateTodo = await TodoModel.findByIdAndUpdate({ _id: req.params.id }, {
            $set: {
                name: req.body.name,
                isDone: req.body.isDone,
                isRemoved: req.body.isRemoved,
                isEdit: req.body.isEdit
            }
        }, { new: true })
        res.json(updateTodo)
    } catch (error) {
        res.json({ message: error })
    }
})

router.put("/multiUpdate/:id", async (req, res) => {
    console.log(req.body)
    try {
        let updateTodo = await TodoModel.findByIdAndUpdate({ _id: req.body.todo._id }, {
            $set: {
                name: req.body.futureTodo.name,
                isDone: req.body.futureTodo.isDone,
                isRemoved: req.body.futureTodo.isRemoved,
                isEdit: req.body.futureTodo.isEdit
            }
        }, { new: true })

        let updateFutureTodo = await TodoModel.findByIdAndUpdate({ _id: req.body.futureTodo._id }, {
            $set: {
                name: req.body.todo.name,
                isDone: req.body.todo.isDone,
                isRemoved: req.body.todo.isRemoved,
                isEdit: req.body.todo.isEdit
            }
        }, { new: true })
        res.json({
            updateFutureTodo, updateFutureTodo
        })
    } catch (error) {
        res.json({ message: error })
    }
})

router.delete("/:id", async (req, res) => {
    try {
        const deleteTodo = await TodoModel.findByIdAndRemove({ _id: req.params.id })
        !!deleteTodo ? res.json(deleteTodo) : res.json({ message: "Cannot find the object" })
    } catch (error) {
        res.json({ message: error })
    }
})

module.exports = router